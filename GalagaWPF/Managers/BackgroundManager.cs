﻿using System;
using System.Collections.Generic;
using System.Linq;
using GalagaWPF.Models;

public class BackgroundManager
{
    private List<Background> backgrounds;
    private DBContext dB = new DBContext();

    public BackgroundManager()
    {

    }
    public List<Background> GetBackgrounds()
    {
        backgrounds = dB.Backgrounds.ToList();
        return backgrounds;
    }



}
