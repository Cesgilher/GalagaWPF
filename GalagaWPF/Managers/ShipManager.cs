﻿using System;
using System.Collections.Generic;
using System.Linq;
using GalagaWPF.Models;

public class ShipManager
{
    private List<Ship> ships;
    private DBContext dB = new DBContext();

    public ShipManager()
    {

    }
    public List<Ship> GetShips()
    {
        ships = dB.Ships.ToList();
        return ships;
    }
    
    
    
}
